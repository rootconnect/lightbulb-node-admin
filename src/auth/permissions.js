import jwt from 'jsonwebtoken'

import { auth, db } from '../firebase/firebase'

export const authenticateUser = async(ctx, next) => {
    if (!ctx.request.header.authorization) ctx.throw(403, 'No token.');
    const token = ctx.request.header.authorization.split(' ')[1];

    console.log("ehere")
    const decoded = await auth.verifyIdToken(token)

    ctx.state.uid = decoded.uid

    await next()
}

export const authenticateMachine = async(ctx, next) => {
    if (!ctx.request.header.authorization) ctx.throw(403, 'No token.');
    const token = ctx.request.header.authorization.split(' ')[1];

    const { machineId } = ctx.params

    const snapshot = await db.ref(`machines/${machineId}/publicKey`).once("value")
    const pKey = snapshot.val()
    ctx.state.machinePublicKey = pKey

	let payload
    try {
		payload = jwt.verify(token, machine.publicKey.replace(/\\n/gm, '\n'), { algorithms: ['RS512']})
	} catch (e) {
        console.log(e)
		if (e instanceof jwt.JsonWebTokenError) {
			// if the error thrown is because the JWT is unauthorized, return a 401 error
			return ctx.throw(401)
		}
		// otherwise, return a bad request error
		return ctx.throw(403)
    }
    
    await next()
    
}
