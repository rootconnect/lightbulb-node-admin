import Router from '@koa/router'

import * as machineController from '../controllers/machine'
import { authenticateUser, authenticateMachine } from '../auth/permissions';

const router = new Router();

router.get('/:code', authenticateUser, machineController.getByCode)

router.post('/', machineController.addNewMachine)

router.post('/:machineId/videos', authenticateMachine, machineController.addVideo)

router.post('/:machineId/inituser', authenticateMachine, machineController.updateMachineUser)

export default router;