import * as admin from 'firebase-admin'

const { DB_URL, FIREBASE_APP_NAME } = process.env

const app = admin.initializeApp({
    credential: admin.credential.applicationDefault(),
    databaseURL: DB_URL,
    storageBucket: `${FIREBASE_APP_NAME}.appspot.com`
});

export const db = app.database()
export const auth = app.auth()
export const storage = app.storage()