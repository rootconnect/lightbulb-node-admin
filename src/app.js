import 'dotenv/config';

import Koa from 'koa'
import logger from 'koa-logger'
import Router from '@koa/router'
import koaBody from 'koa-body'
import cors from '@koa/cors'

import machineRoutes from './routes/machine'

const router = new Router()
const app = new Koa();
const PORT = process.env.PORT || 3001;
app.proxy = true;

app.use(logger());
app.use(koaBody({
    multipart: true,
    formidable: {
        maxFileSize:200 * 1024 * 1024,
        keepExtensions: true, //  Extensions to save images
    }
}));
app.use(cors());

app.use(async(ctx,next) => {
    console.log("here")
    await next()
})

router.use('/machines', machineRoutes.routes())


app.use(router.routes());

// app.on('error', (err, ctx) => {
//   console.error('server error', err, ctx)
// });


app.listen(PORT, () => {
    console.log(`Server listening on port: ${PORT}`);
});