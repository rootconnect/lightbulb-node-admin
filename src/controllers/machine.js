import { db, storage } from '../firebase/firebase'
import fs from 'fs'
import shortid from 'shortid'
import { promisify } from 'util'
import ffmpeg from 'fluent-ffmpeg'
import { v1 as uuidv1 } from 'uuid';

const readFile = promisify(fs.readFile)

export const getByCode = async(ctx, next) => {

    const { code } = ctx.params
    const { uid } = ctx.state

    
    const machineSnapshots = await db.ref("machines").orderByChild("code").equalTo(code).once("value")
    let ref = null
    let m = null
    let refKey = null

    machineSnapshots.forEach(snap => {
        ref = snap.ref
        refKey = ref.key
        m = snap.val()
    })
    
    if(!machineSnapshots.exists()){
        ctx.throw(403, "Forbidden")
    } else {

        if(m.activated){
            ctx.throw(403, "Forbidden")
        }

        await ref.update({
            activated: true,
            users: {...(m.users ? m.users: {}), [uid]: true}
        })

        await db.ref("groups/"+refKey).set({
            admins: {
                [ctx.state.uid]: true
            }
        })

        await db.ref("users/"+uid).set({
            machineId: refKey
        })

        ctx.body = {
            "success": true
        }
    }


    await next()
}

export const updateMachineUser = async(ctx, next) => {
    const { machineId } = ctx.params
    await db.ref("machines").child(machineId).update({
        machineUser: ctx.request.body.userId
    })

    ctx.body = {
        success: true 
    }

    await next()
}

export const addNewMachine = async(ctx, next) => {

    const { key } = ctx.request.files

    const f = await readFile(key.path)

    const newMachineRef = await db.ref("machines").push()
    const code = shortid.generate()

    await newMachineRef.set({
        publicKey: f.toString("utf8").replace(/\n$/, ''),
        code
    })

    ctx.body = {
        machineId: newMachineRef.key,
        code
    }
}

export const addVideo = async(ctx, next) => {
    const { machineId } = ctx.params
    const { machine } = ctx.state

    console.log(ctx.request.files)
    const { video } = ctx.request.files


    const timestamp = new Date().getTime()
    const uniqueId = uuidv1()
    const vidToken = uuidv1()

    const upload = await storage.bucket().upload(video.path, {
        destination: `videos/${machineId}/${uniqueId}`,
        metadata: {
            metadata: {
                firebaseStorageDownloadTokens: vidToken
            }
        }
    })
    

    let downloadUrl = `https://firebasestorage.googleapis.com/v0/b/${storage.bucket().name}/o/${encodeURIComponent(`videos/${machineId}/${uniqueId}`)}?alt=media&token=${vidToken}`

    // ffmpeg(video.path)
    // .on('filenames', function(filenames) {
    //     console.log('Will generate ' + filenames.join(', '))
    // })
    // .on('end', function() {
    //     console.log('Screenshots taken');
    // })
    // .screenshots({
    //     // Will take screens at 20%, 40%, 60% and 80% of the video
    //     count: 4,
    //     folder: `/tmp/${video.name}`
    // });
    try {
        await db.ref("machines").child(machineId).update({
            videos: {
                ...(machine.videos ? machine.videos : {}),
                [uniqueId]:{
                    timestamp,
                    publicUrl: downloadUrl
                }
            }
        })

    } catch(e){
        console.log("error", e)
    }

    ctx.body = {
        success: true
    }
}